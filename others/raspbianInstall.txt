STRESS TEST AND OVERCLOCK
https://core-electronics.com.au/tutorials/stress-testing-your-raspberry-pi.html
https://haydenjames.io/stop-overclock-raspberry-pi-3-b-plus/

BUSTER
https://downloads.raspberrypi.org/raspbian_lite_latest
STRETCH:
https://downloads.raspberrypi.org/raspbian_lite/images/raspbian_lite-2019-04-09/2019-04-08-raspbian-stretch-lite.zip


habilitar vnc, ssh e ftp/https
instalar python 3.6.8 e pip

PIGPIO
https://raspberrypi.stackexchange.com/questions/70568/how-to-run-pigpiod-on-boot

PCA9685
https://github.com/adafruit/Adafruit_Python_PCA9685
https://learn.adafruit.com/16-channel-pwm-servo-driver/python-circuitpython


OPENCV 4.1.0
https://www.pyimagesearch.com/2018/09/26/install-opencv-4-on-your-raspberry-pi/
https://gist.github.com/willprice/abe456f5f74aa95d7e0bb81d5a710b60