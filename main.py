# executar com:
# nohup python3 main.py &

import cv2
import constants as const
from functions import *
from classes import Framerate
import time

if const.ON_RPI_CAM:
    from classes import RaspiCam as Camera
else:
    from classes import USBCam as Camera

if const.ON_RPI:
    import pigpio
    from classes import Warning_LED, Digital_GPIO, UltrassonicSensor, H_Bridge, CameraMovement, RoboticArm
    from adafruit_servokit import ServoKit
    import Adafruit_PCA9685

if __name__ == "__main__":

    if const.ON_RPI:

        # Configuração de GPIO
        gpio = pigpio.pi()

        if const.ON_RPI_DRIVE:
            # Configuração de driver de servo PCA9685
            pca = Adafruit_PCA9685.PCA9685()
            servo_kit = ServoKit(channels=16)
            pca.set_pwm_freq(100)

            # Braço e garra robótica
            robotic_arm = RoboticArm(
                servo_kit, const.ARM_SERVO_PINS, const.ARM_SERVO_POSITIONS)

            # Câmera
            camera = CameraMovement(
                servo_kit, const.CAMERA_SERVO_PIN, const.CAMERA_SERVO_POSITIONS)

            motors = H_Bridge(const.L298N_MOTORS, gpio, pca)

            robotic_arm.reset()
            camera.down()

        # Pino correspondente ao interruptor
        standby = Digital_GPIO("input", gpio, const.RPI_STANDBY, invert=True)
        # Pino correspondente ao pushbuttom
        push_bttm = Digital_GPIO("input", gpio, const.RPI_PUSH_BTTM)
        # Objeto de leds de aviso
        warning = Warning_LED(gpio, const.RPI_LEDS)
        # Fita de LEDS iluminação
        led_strip = Digital_GPIO(
            "output", gpio, const.RPI_LED_RELAY, invert=True)
        # Sensor de inclinação
        inclination = Digital_GPIO(
            "input", gpio, const.RPI_INCLINATION_SENSOR)

        ultra_sonic_front = UltrassonicSensor(
            gpio, const.RPI_F_US_Trigger, const.RPI_F_US_ECHO).start()

    if const.RECORD_VIDEO:
        video = cv2.VideoWriter('recordings/'+time.strftime("%d-%m-%Y_%X")+'.avi', cv2.VideoWriter_fourcc(
            'M', 'J', 'P', 'G'), 24, (const.FRAME_WIDTH, const.FRAME_HEIGHT), True)

    capture = Camera(const.FRAME_WIDTH, const.FRAME_HEIGHT).start()
    fps = Framerate()

    last_bottom_line = (int(const.FRAME_WIDTH/2), const.FRAME_WIDTH)
    previous_intersection_direction = False
    previous_intersection_count = 0

    previous_circle = []
    previous_circle_timestamp = 0
    ball_counter = 0

    previous_ultrasonic_distance = 100

    inclination_start_timestamp = False
    on_ramp = False
    ramp = False

    rescue_area = False

    while True:

        try:

            if const.ON_RPI:

                if push_bttm.read():
                    fps.isStandby()
                    bttm_time_start = time.time()

                    while push_bttm.read():
                        pass

                    delta_time = time.time() - bttm_time_start

                    if delta_time > 0.1:
                        if delta_time > 1.5:
                            rescue_area = not rescue_area
                            if const.ON_RPI:
                                if rescue_area:
                                    led_strip.write(True)
                                else:
                                    led_strip.write(False)
                        else:
                            last_bottom_line = (
                                int(const.FRAME_WIDTH/2), const.FRAME_WIDTH)
                            previous_intersection_direction = False
                            previous_intersection_count = 0

                            previous_circle = []
                            previous_circle_timestamp = 0
                            previous_ultrasonic_distance = 100
                            ball_counter = 0

                if inclination.read():
                    on_ramp = True
                    inclination_timestamp = time.time()
                    if not inclination_start_timestamp:
                        inclination_start_timestamp = inclination_timestamp
                    elif inclination_timestamp - inclination_start_timestamp >= 10:
                        ramp = True
                elif inclination_start_timestamp:
                    on_ramp = False
                    inclination_start_timestamp = False
                else:
                    on_ramp = False

            if const.ON_RPI and standby.read():

                last_bottom_line = (
                    int(const.FRAME_WIDTH/2), const.FRAME_WIDTH)
                previous_intersection_direction = False
                previous_intersection_count = 0

                previous_circle = []
                previous_circle_timestamp = 0
                previous_ultrasonic_distance = 100
                ball_counter = 0
                fps.isStandby()
                led_strip.write(False)

                if const.ON_RPI_DRIVE:
                    motors.reset()

            else:
                if const.ON_RPI and const.ON_RPI_DRIVE:
                    if on_ramp:
                        motors.setMultiplier(const.L298N_RAMP_MULT)
                    elif rescue_area:
                        motors.setMultiplier(const.L298_RESCUE_MULT)
                    else:
                        motors.setMultiplier(const.L298N_DEFAULT_MULT)

                frame = capture.getFrame()

                if not len(frame):
                    continue

                if const.IMG_SHOW or const.RECORD_VIDEO:
                    output = frame.copy()

                if rescue_area:

                    circle = scanCircles(frame, previous_circle)

                    if len(circle) and (const.IMG_SHOW or const.RECORD_VIDEO):
                        output = cv2.circle(
                            output, (circle[0], circle[1]), circle[2], (0, 255, 0), 3)

                    # ROTINA DE RESGATAR VITIMAS
                    if ball_counter >= 3:
                        coord_area, contour = get_rescue_area_x(frame, circle)

                        if coord_area is not None:
                            area_x = coord_area[0] - const.FRAME_WIDTH/2
                            if const.IMG_SHOW or const.RECORD_VIDEO:
                                output = cv2.drawContours(
                                    output, contour, -1, (0, 255, 0))
                                output = drawTarget(
                                    output, coord_area[0], coord_area[1], (255, 255, 255))

                        if const.ON_RPI_DRIVE:
                            if coord_area is None:
                                motors.run(-100, 100)
                            else:
                                distance = ultra_sonic_front.distance
                                if distance <= 5:
                                    motors.turn180('right')
                                    motors.run(-100, -100)

                                    time.sleep(2)

                                    motors.reset()
                                    robotic_arm.box_up()
                                    time.sleep(8)
                                    ball_counter = 0
                                    robotic_arm.box_down()

                                elif abs(area_x) <= 20 / const.PX_DIVISOR:
                                    motors.run(100, 100)

                                elif area_x > 0:
                                    motors.run(100, 0)

                                elif area_x < 0:
                                    motors.run(0, 100)

                    else:

                        # ROTINA ENCONTRAR VITIMAS
                        if not len(circle) or not circle[0]:
                            if const.ON_RPI:
                                distance = ultra_sonic_front.distance

                                # PROCURAR Á DIREITA
                                if const.ON_RPI_DRIVE:
                                    if distance is None or distance < 8:
                                        motors.turn90('right')
                                    elif distance < 20:
                                        motors.run(0, 100)
                                    else:
                                        motors.run(100, 100)

                            this_timestamp = time.time()

                            if previous_circle_timestamp and this_timestamp - previous_circle_timestamp > 0.7:
                                previous_circle = []
                                previous_circle_timestamp = 0

                        # ROTINA RASTREAR VÍTIMA E GUARDAR
                        else:
                            previous_circle_timestamp = time.time()
                            previous_circle = circle
                            (x, y, r) = circle

                            offcenter = x - const.FRAME_WIDTH/2

                            if const.ON_RPI:
                                distance = ultra_sonic_front.distance

                                if previous_ultrasonic_distance < 4.5 and distance is None:
                                    if const.ON_RPI_DRIVE:
                                        robotic_arm.grab_and_store(motors)
                                        ball_counter += 1
                                    previous_ultrasonic_distance = 100
                                else:
                                    if distance is not None:
                                        previous_ultrasonic_distance = distance

                            if const.IMG_SHOW or const.RECORD_VIDEO:
                                cv2.putText(output, str(offcenter)+"px", (10, 60),
                                            cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)

                # NÃO ESTÁ NO RESGATE
                else:

                    # ROTINA DE OBSTÁCULO
                    if const.ON_RPI:
                        distance = ultra_sonic_front.distance
                        if previous_ultrasonic_distance <= 10 and distance is None and const.ON_OBSTACLE:
                            if const.ON_RPI_DRIVE:
                                motors.turn90("right")
                                motors.run(100, 100)
                                time.sleep(1.7)
                                motors.turn90("left")
                                motors.run(100, 100)
                                time.sleep(2)
                                motors.turn90("left")
                                motors.run(100, 100)
                                time.sleep(1.7)
                                motors.turn90("right")
                        elif distance is not None:
                            previous_ultrasonic_distance = distance

                    green_selection = getGreenContours(frame)
                    try:
                        raise_Y_max = previous_intersection_count >= 2 and not previous_intersection_direction == "error"
                        first_point, last_point, fp_width, intersection = detectPath(
                            frame, last_bottom_line, green_selection, raise_Y_max=raise_Y_max)

                    # ROTINA DE MOVIMENTO SOBRE INTERSECÇÃO
                    except IntersectionOverMaxY:
                        direction = previous_intersection_direction
                        print("Intersection, continue "+str(direction))
                        if const.ON_RPI_DRIVE:
                            motors.run(100, 100)
                            time.sleep(const.DP_ON_INTERSECTION_TIME)
                            if direction == 'back':
                                motors.turn180('left')
                            elif direction == 'right':
                                motors.turn90('right')
                            elif direction == 'left':
                                motors.turn90('left')
                            elif direction == 'front':
                                motors.run(100, 100)
                                time.sleep(0.1)
                            else:
                                motors.run(100, 100)
                            # pausa
                            motors.reset()
                            time.sleep(0.5)
                            continue

                    if fp_width:

                        # INTERSECÇÃO ENCONTRADA, GUARDAR DADOS
                        if intersection:
                            all_greens = greenScan(green_selection)
                            try:
                                direction, green = decideIntersection(
                                    intersection, all_greens)
                            except DecisionError as warn:
                                print(warn)
                                direction = "error"
                                green = []

                            if not previous_intersection_direction:
                                previous_intersection_count = 1
                                previous_intersection_direction = direction
                            elif direction == previous_intersection_direction:
                                previous_intersection_count = 1 + previous_intersection_count
                            else:
                                previous_intersection_count = 1
                                previous_intersection_direction = direction

                            if const.IMG_SHOW or const.RECORD_VIDEO:
                                cv2.putText(output, direction, (30, const.FRAME_HEIGHT - 30),
                                            cv2.FONT_HERSHEY_SIMPLEX, 1.5, (160, 9, 180), 2)

                                if len(all_greens):
                                    for (x, y) in all_greens:
                                        if (x, y) != any(green):
                                            output = drawTarget(
                                                output, x, y, (0, 0, 255))

                                    if len(green):
                                        for (x, y) in green:
                                            output = drawTarget(
                                                output, x, y, (0, 255, 0))

                                output = cv2.circle(
                                    output, (intersection["x"], intersection["y"]), 2, (255, 255, 255), 4)
                                if intersection["front"]:
                                    cv2.putText(output, "F", (const.FRAME_WIDTH - 18, 20),
                                                cv2.FONT_HERSHEY_SIMPLEX, 0.7, (255, 0, 0), 2)
                                if intersection["left"]:
                                    cv2.putText(output, "L", (const.FRAME_WIDTH - 18, 40),
                                                cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 255, 0), 2)
                                if intersection["right"]:
                                    cv2.putText(output, "R", (const.FRAME_WIDTH - 18, 60),
                                                cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 0, 255), 2)
                        else:
                            previous_intersection_direction = False
                            if const.IMG_SHOW or const.RECORD_VIDEO:
                                cv2.putText(output, "No intersection", (10, 90),
                                            cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2)

                        angle = getAngle(first_point, last_point)
                        offcenter = int(
                            round(((first_point[0] + last_point[0]) / 2) - const.FRAME_WIDTH/2))
                        last_bottom_line = (first_point[0], fp_width)

                        if const.IMG_SHOW or const.RECORD_VIDEO:
                            output = cv2.line(output, tuple(
                                first_point), tuple(last_point), (0, 255, 0), 3)
                            cv2.putText(output, str(angle)+"deg", (10, 30),
                                        cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)
                            cv2.putText(output, str(offcenter)+"px", (10, 60),
                                        cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)

                        # ROTINA DE SEGUIR LINHA
                        if const.ON_RPI_DRIVE:
                            if angle > 45:
                                if offcenter < -200 / const.PX_DIVISOR:
                                    motors.run(100, 100)
                                else:
                                    motors.run(-100, 100)

                            elif angle < -45:
                                if offcenter < 200 / const.PX_DIVISOR:
                                    motors.run(100, 100)
                                else:
                                    motors.run(100, -100)

                            elif angle > 15:
                                if offcenter < -100 / const.PX_DIVISOR:
                                    motors.run(100, 100)
                                else:
                                    motors.run(0, 100)

                            elif angle < -15:
                                if offcenter < 100 / const.PX_DIVISOR:
                                    motors.run(100, 100)
                                else:
                                    motors.run(100, 0)

                            elif offcenter > 100 / const.PX_DIVISOR:
                                motors.run(0, 100)

                            elif offcenter < -100 / const.PX_DIVISOR:
                                motors.run(100, 0)

                            else:
                                motors.run(100, 100)

                    else:
                        if ramp:
                            rescue_area = True
                            if const.ON_RPI:
                                led_strip.write(True)
                                if const.ON_RPI_DRIVE:
                                    camera.up()

                        if const.ON_RPI_DRIVE:
                            motors.run(100, 100)

                        if const.IMG_SHOW or const.RECORD_VIDEO:
                            cv2.putText(output, "No Line", (10, 30),
                                        cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2)

                fps.setNewFrame()

                if const.RECORD_VIDEO:
                    video.write(cv2.resize(
                        output, (const.FRAME_WIDTH, const.FRAME_HEIGHT)))

                if const.IMG_SHOW:
                    cv2.imshow("", output)
                    if cv2.waitKey(1) & 0xFF == ord("q"):
                        break

        except KeyboardInterrupt:
            print("Terminando")
            break
        # except Exception as e:
        #    print(e)
        #    continue

    print("FPS: "+str(fps.finalFPS()))
    capture.stop()
    cv2.destroyAllWindows()

    if const.RECORD_VIDEO:
        video.release()

    if const.ON_RPI:
        ultra_sonic_front.stop()
        led_strip.write(False)

        if const.ON_RPI_DRIVE:
            robotic_arm.release()
            camera.release()
            motors.reset()
        gpio.stop()
