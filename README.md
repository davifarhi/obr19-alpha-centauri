# Alpha Centauri OBR 2019

Software do robô produzido pela equipe **Alpha Centauri**, sub-equipe da **Equipe Genesis** do **IFTO - Campus Palmas** para participação na etapa estadual (Tocantins) da **Olimpíada Brasileira de Robótica 2019**. O projeto foi premiado com a medalha de Inovação.

A razão para este projeto de alto nível de dificuldade foi um interesse por parte da equipe de mudar como os robôs de competição são idealizados e feitos, geralmente as equipes utilizam kits didáticos simples de montar e de programar, nossa vontade era de deixar o processo inteiro mais interessante e desafiador. Então tivemos a ideia de usar câmera e **OPENCV** (uma biblioteca completa e fácil de usar de **Computação Visual**) para reconhecer as linhas, as intersecções e as vítimas a serem resgatadas.

Essa inovação trouxe a mim, programador da equipe, muito trabalho, pois um código para analisar milhares de pixels de uma imagem é muito mais complicado que o código normal, que só recebe alguns bits dos sensores utilizados. E, além disso, eu não possuía nenhuma experiência prévia com Computação Visual. Imagino que dê para visualizar o trabalho que deu para desenvolver os algoritmos de reconhecimento necessários. Curiosamente, o mais desafiador dentre eles foi o algoritmo de identificação de intersecções e é principalmente esse que deve ser testado e "lapidado" para a próxima versão do robô.

Nossa performance na OBR não chegou nem perto da performance desejada, porém atribuímos isso à falta de testes anteriormente à competição e ao extremamente curto prazo que tivemos para montar o robô. Porém não estou desanimado e já estamos planejando o que podemos fazer para melhorar a próxima versão e termos uma chance real na próxima competição.

Desenvolvedor: **Davi Farhi** - [@Gmail](davifarhi@gmail.com)