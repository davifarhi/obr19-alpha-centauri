"""FUNÇÕES ADICIONAIS"""

import cv2
import math
import numpy as np
import constants as const


def getAngle(p0, p1):
    """Calcula o ângulo tangente entre os dois pontos"""
    if p0[1] - p1[1] != 0:
        return int(round(math.degrees(math.atan((p1[0] - p0[0]) / (p0[1] - p1[1])))))
    return 0


def getDistance(p0, p1):
    """Calcula a distância entre dois pontos"""
    return abs(math.hypot(p1[0] - p0[0], p1[1] - p0[1]))


def drawTarget(image, x, y, color):
    """Desenha uma retícula no ponto indicado"""
    cv2.circle(image, (x, y), 10, color, 1)

    if(y-15 > 0):
        cv2.line(image, (x, y), (x, y-15), color, 1)
    else:
        cv2.line(image, (x, y), (x, 0), color, 1)
    if(y+15 < const.FRAME_HEIGHT):
        cv2.line(image, (x, y), (x, y+15), color, 1)
    else:
        cv2.line(image, (x, y), (x, const.FRAME_HEIGHT), color, 1)
    if(x-15 > 0):
        cv2.line(image, (x, y), (x-15, y), color, 1)
    else:
        cv2.line(image, (x, y), (0, y), color, 1)
    if(x+15 < const.FRAME_WIDTH):
        cv2.line(image, (x, y), (x+15, y), color, 1)
    else:
        cv2.line(image, (x, y), (const.FRAME_WIDTH, y), color, 1)

    return image


def getGreenContours(image):
    hsv_image = cv2.cvtColor(image, cv2.COLOR_RGB2HSV)

    bin_img = cv2.inRange(hsv_image, const.GS_HSV_MIN, const.GS_HSV_MAX)
    bin_img = cv2.erode(
        bin_img, const.GS_ERODE_KERNEL, iterations=const.GS_ITERATIONS)
    return bin_img

def greenScan(bin_img):
    """Função que seleciona as áreas verdes na imagem e identifica contornos"""

    contours, _ = cv2.findContours(
        bin_img, cv2.RETR_CCOMP, cv2.CHAIN_APPROX_SIMPLE)

    green_contours = []

    if len(contours):

        for contour in contours:
            moment = cv2.moments(contour)
            green_area = moment["m00"]

            if green_area > const.GS_AREA_MIN:
                x = moment["m10"] / green_area
                y = moment["m01"] / green_area

                green_contours.append((x, y))

    if not len(green_contours):
        return []

    for i, contour in enumerate(green_contours):
        green_contours[i] = (int(contour[0]), int(contour[1]))

    return green_contours


def linesDifference(this_line, last_line):
    """Calcula por quantos pixels as duas linhas estão separadas
    (Calculado a partir da ponta de cada linha"""

    this_start = this_line[0] - int(this_line[1]/2)
    this_end = this_line[0] + int(this_line[1]/2)

    last_start = last_line[0] - int(last_line[1]/2)
    last_end = last_line[0] + int(last_line[1]/2)

    if this_end < last_start:
        return last_start - this_end
    if last_end < this_start:
        return this_start - last_end
    return 0


def rowScan(row, compare_x, compare_w):
    """Busca por uma linha no vetor de bits"""
    start = None
    end = None
    tolerance = 0
    line = []
    found_line = False

    for i, value in enumerate(row):
        if value:
            if not start:
                start = i

            tolerance = const.DP_PX_TOLERANCE + 1
            end = i

        elif start and tolerance:
            tolerance = tolerance - 1

        if start and ((not tolerance and not value) or i == (len(row)-1)):
            if end - start >= const.DP_LINE_WIDTH_MINIMUM:
                line_x = int(round((start+end)/2))
                line_w = end - start + 1
                line.append((abs(compare_x - line_x), line_x, line_w))
                found_line = True

            end = None
            start = None

    if found_line:
        line = min(line)
        line = line[:][1:]
        if linesDifference(line, (compare_x, compare_w)) <= const.DP_LINES_MAX_DIFFERENCE:
            return line
    return False, False


def scanLeftRight(bin_img, intersection):
    """Scaneia os dois lados da intersecção para identificar se há
    um caminho ou não

    Retorna booleano "direita" e "esquerda" """

    first_y, first_w = rowScan(
        [row[intersection[0]] for row in bin_img], intersection[1], const.DP_LINE_WIDTH_MAXIMUM)

    if not first_w:
        return False, False

    prev_y = first_y
    prev_w = first_w

    right = False
    left = False

    line_counter = 0

    limit = intersection[0] - 1 - const.DP_LINE_WIDTH_MAXIMUM
    if limit < 0:
        limit = 0
    for x in reversed(range(limit, intersection[0], const.DP_ROW_RANGE)):
        line_center, line_width = rowScan(
            [row[x] for row in bin_img], prev_y, prev_w)
        if 0 < line_width < const.DP_LINE_WIDTH_MAXIMUM:
            if not linesDifference(
                    (line_center, line_width), (first_y, first_w)):
                if line_counter >= int((const.DP_LINE_WIDTH_MINIMUM * const.DP_L_R_MULT)/const.DP_ROW_RANGE):
                    left = True
                    break
                line_counter = line_counter + 1
                prev_y = line_center
                prev_w = line_width

        else:
            line_counter = 0

    prev_y = first_y
    prev_w = first_w

    line_counter = 0

    limit = intersection[0] + 1 + const.DP_LINE_WIDTH_MAXIMUM
    if limit >= const.FRAME_WIDTH:
        limit = const.FRAME_WIDTH - 1
    for x in range(intersection[0] + 1, limit, const.DP_ROW_RANGE):
        line_center, line_width = rowScan(
            [row[x] for row in bin_img], prev_y, prev_w)

        if 0 < line_width < const.DP_LINE_WIDTH_MAXIMUM:
            if not linesDifference(
                    (line_center, line_width), (first_y, first_w)):
                if line_counter >= int((const.DP_LINE_WIDTH_MINIMUM * const.DP_L_R_MULT)/const.DP_ROW_RANGE):
                    right = True
                    break
                line_counter = line_counter + 1
                prev_y = line_center
                prev_w = line_width

        else:
            line_counter = 0

    return left, right


class IntersectionOverMaxY(Exception):
    pass


class MotorDeciding(Exception):
    pass


def detectPath(image, prev_line, green_detection, raise_Y_max=False):
    """Scaneia a imagem em busca de linhas e intersecções"""

    black_selection = cv2.inRange(
        image, const.DP_BLACK_BGR_MIN, const.DP_BLACK_BGR_MAX)
    black_selection = cv2.erode(
        black_selection, const.DP_ERODE_KERNEL, iterations=const.DP_ERODE_ITERATIONS)

    black_selection = np.subtract(black_selection, green_detection)

    first_line = []
    first_line_width = False
    intersection = {}
    max_width_line_counter = 0

    for y in reversed(range(0, const.FRAME_HEIGHT, const.DP_ROW_RANGE)):

        line_center, line_width = rowScan(
            black_selection[y][:], prev_line[0], prev_line[1])

        if line_width:

            if "y" in intersection and max_width_line_counter > int((const.DP_LINE_WIDTH_MINIMUM * 1.5)/const.DP_ROW_RANGE) and line_width <= const.DP_LINE_WIDTH_MAXIMUM:
                intersection["x"] = int((prev_line[0] + line_center) / 2)
                intersection["front"] = True
                break

            if line_width > const.DP_LINE_WIDTH_MAXIMUM:
                if not intersection:
                    if max_width_line_counter >= (const.DP_LINE_WIDTH_MINIMUM * 2 / 3)/const.DP_ROW_RANGE and first_line:
                        intersection["y"] = y
                        max_width_line_counter = 0
                    else:
                        max_width_line_counter = max_width_line_counter + 1
            else:
                if intersection:
                    max_width_line_counter = max_width_line_counter + 1
                else:
                    prev_line = [line_center, y]
                    max_width_line_counter = 0

                if not first_line:
                    first_line = [line_center, y]
                    first_line_width = line_width

    if intersection:
        intersection["left"] = False
        intersection["right"] = False
        if not "front" in intersection:
            intersection["front"] = False
        if not "x" in intersection:
            intersection["x"] = prev_line[0]

        intersection["left"], intersection["right"] = scanLeftRight(
            black_selection, (intersection["x"], intersection["y"]))

        prev_line = (intersection["x"],
                     intersection["y"]+int(12/const.PX_DIVISOR))

        if raise_Y_max and intersection["y"] > const.DP_MAX_Y_INTERSECTION:
            raise IntersectionOverMaxY

    return first_line, prev_line, first_line_width, intersection


class DecisionError(Exception):
    pass


def decideIntersection(intersection, greens):
    """Retorna a decisão de qual sentido o robô deve seguir na intersecção"""

    if not (intersection["front"] or intersection["left"] or intersection["right"]):
        raise DecisionError(
            "Intersection detected but no R, L or F path found")
    if len(greens):
        new_greens = []
        for (x, y) in greens:
            if not (y < intersection["y"] or getDistance((x, y), (intersection["x"], intersection["y"])) > const.DI_MAXIMUM_G2I_DISTANCE):
                new_greens.append((x, y))
        greens = new_greens

    if not len(greens):
        if intersection["front"]:
            return "front", []
        if intersection["right"] and intersection["left"]:
            # nao deve acontecer
            raise DecisionError(
                "No green rectangle, no F path, and R and L paths found")
        if intersection["right"]:
            return "right", []
        if intersection["left"]:
            return "left", []

    if len(greens) > 2:
        temp_greens = []
        for green in greens:
            temp_greens.append((green, getDistance(
                green, (intersection["x"], intersection["y"]))))

        temp_greens.sort()

        for i in range(2, len(temp_greens)):
            greens.remove(temp_greens[i][0])

    green_right = False
    green_left = False
    for (x, y) in greens:
        if x < intersection["x"]:
            green_left = True
        else:
            green_right = True

    if green_left and green_right:
        return "back", greens
    if green_left:
        return "left", greens
    if green_right:
        return "right", greens

    return "front", []


def getCircles(image, min_radius=const.DC_RADIUS_MIN):
    """Detecta os círculos presentes no frame e retorna suas coordenadas e raio"""
    bin_img = cv2.inRange(image, (0, 0, 0), const.DC_MAX_BGR)
    bin_img = cv2.GaussianBlur(
        bin_img, (const.DC_BLUR_VALUE, const.DC_BLUR_VALUE), cv2.BORDER_DEFAULT)
    circles = cv2.HoughCircles(bin_img, cv2.HOUGH_GRADIENT, const.DC_PARAM_DP,
                               const.DC_D_BETWEEN, param2=const.DC_PARAM2, minRadius=min_radius, maxRadius=const.DC_RADIUS_MAX)
    if circles is not None:
        circles = np.round(circles[0, :]).astype("int")
    return circles


def firstElement(elem):
    return elem[0]


def scanCircles(image, last_circle):
    """Scaneia a imagem em busca de círculos e seleciona o mais apropriado"""
    had_circle = bool(len(last_circle))

    if had_circle:

        circles = getCircles(
            image, last_circle[2] - const.RA_RADIUS_VARIATION)

        if circles is None:
            return []

        new_circles = []

        for circle in circles:
            (x, y, r) = circle
            points = abs(r - last_circle[2]) + \
                getDistance((x, y), (last_circle[0], last_circle[1]))

            if points < const.RA_MAX_POINTS:
                new_circles.append((points, circle))

        if not len(new_circles):
            return []

        new_circles.sort()
        return new_circles[0][1]

    else:

        circles = getCircles(image, const.DC_RADIUS_MIN)

        if circles is None:
            return []

        new_circles = []

        for circle in circles:
            (x, y, r) = circle
            new_circles.append((r, circle))

        if not len(new_circles):
            return []

        new_circles.sort(reverse=True, key=firstElement)

        return new_circles[0][1]


def get_rescue_area_x(image, circle):
    bin_img = cv2.inRange(image, const.RA_BGR_MIN,
                          const.RA_BGR_MAX)
    bin_img = cv2.erode(bin_img, const.RA_ERODE_KERNEL,
                        iterations=const.RA_ITERATIONS)

    contours, _ = cv2.findContours(
        bin_img, cv2.RETR_CCOMP, cv2.CHAIN_APPROX_SIMPLE)

    black_contours = []

    if len(contours):

        for contour in contours:
            moment = cv2.moments(contour)
            black_area = moment["m00"]

            if black_area > const.RA_BLACK_MIN_AREA:
                x = int(moment["m10"] / black_area)
                y = int(moment["m01"] / black_area)

                if len(circle) and getDistance((circle[0], circle[1]), (x, y)) < circle[2]:
                    continue

                black_contours.append((black_area, (x, y), contour))

    if not len(black_contours):
        return None, None

    black_contours.sort(reverse=True)

    return black_contours[0][1:3]
