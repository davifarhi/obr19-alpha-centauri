import time
import cv2
import numpy as np

# Cria a instância de captura das imagens da camera
capture = cv2.VideoCapture(0)

# Determina atributos da imagem
capture.set(3, 640)		# Largura
capture.set(4, 480)		# Altura
capture.set(5, 30)		# FPS
capture.set(10, 100)	# Luminosidade

successful = True

# Laço principal (roda até haver uma falha na captura ou até que o usuário aperte 'q')
while successful:

	# Captura um frame da camera
	successful, image = capture.read()

	# Cria uma imagem com parte do frame total
	roi = image[200:250, :]

	# Seleciona as áreas com cor preta (linha)
	Blackline = cv2.inRange(roi, (0, 0, 0), (50, 50, 50))

	kernel = np.ones((3, 3), np.uint8)

	# Corta rúidos da seleção das cores pretas
	Blackline = cv2.erode(Blackline, kernel, iterations=5)
	Blackline = cv2.dilate(Blackline, kernel, iterations=9)

	# Contorna a área de seleção
	contours_blk, hierarchy_blk = cv2.findContours(Blackline, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

	# Caso haja uma área preta, desenha-se uma linha que passa no meio de um retangulo que contém a area selecionada
	if len(contours_blk) > 0:
		x_blk, y_blk, w_blk, h_blk = cv2.boundingRect(contours_blk[0])
		centerx_blk = x_blk + (int(w_blk/2))
		cv2.line(image, (centerx_blk, 200), (centerx_blk, 250), (255, 0, 0), 3)

		# Mostra na tela a distancia (em px) horizontal da linha desenhada ao meio da imagem
		setpoint = 320
		error = centerx_blk - setpoint
		centertext = "Error = " + str(error)
		cv2.putText(image, centertext, (200,340), cv2.FONT_HERSHEY_SIMPLEX, 1.5, (255,0,0),3)

	# Mostra a imagem com o texto e a linha
	cv2.imshow("video", image)

	key = cv2.waitKey(1) & 0xFF
	if key == ord("q"):
		break

capture.release()
cv2.destroyAllWindows()