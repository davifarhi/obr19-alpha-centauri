import time
import cv2
import numpy as np

# Cria a instância de captura das imagens da camera
capture = cv2.VideoCapture(0)

# Determina atributos da imagem
capture.set(3, 640)		# Largura
capture.set(4, 480)		# Altura
capture.set(5, 30)		# FPS
capture.set(10, 100)	# Luminosidade

successful = True

# Laço principal (roda até haver uma falha na captura ou até que o usuário aperte 'q')
while successful:

	# Captura um frame da camera
    successful, image = capture.read()

	# Cria uma imagem com parte do frame total
    roi = image[200:250, :]

	# Seleciona as áreas com cor preta (linha)
    Blackline = cv2.inRange(roi, (0, 0, 0), (50, 50, 50))

	# Seleciona as áreas com cor verde (quadrado verde) e cria variável que determina se há verde
    Greensign = cv2.inRange(image, (0, 65, 0), (100, 200, 100))
    Greendected = False

    kernel = np.ones((3, 3), np.uint8)

	# Corta rúidos da seleção das cores pretas
    Blackline = cv2.erode(Blackline, kernel, iterations=5)
    Blackline = cv2.dilate(Blackline, kernel, iterations=9)

	# Corta rúidos da seleção das cores verdes
    Greensign = cv2.erode(Greensign, kernel, iterations=5)
    Greensign = cv2.dilate(Greensign, kernel, iterations=9)

	# Contorna as áreas de seleção
    contours_blk, hierarchy_blk = cv2.findContours(
        Blackline, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    contours_grn, hierarchy_grn = cv2.findContours(
        Greensign, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

	# Caso haja uma área verde, desenha-se uma linha que passa no meio de um retangulo que contém a area selecionada
    if len(contours_grn) > 0:
        Greendected = True
        x_grn, y_grn, w_grn, h_grn = cv2.boundingRect(contours_grn[0])
        centerx_grn = x_grn + (int(w_grn/2))
        cv2.line(image, (centerx_grn, 200),
                 (centerx_grn, 250), (0, 0, 255), 3)

	# Caso haja uma área preta, desenha-se uma linha que passa no meio de um retangulo que contém a area selecionada
    if len(contours_blk) > 0:
        x_blk, y_blk, w_blk, h_blk = cv2.boundingRect(contours_blk[0])
        centerx_blk = x_blk + (int(w_blk/2))
        cv2.line(image, (centerx_blk, 200), (centerx_blk, 250), (255, 0, 0), 3)

    if Greendected :
        # Mostra na tela o comportamento que o robô deve ter em relação ao quadrado verde
        if centerx_grn > centerx_blk :
	        cv2.putText(image, "Turn Right", (350,180), cv2.FONT_HERSHEY_SIMPLEX, 1.5, (0,255,0),3)
        else :
	        cv2.putText(image, "Turn Left", (50,180), cv2.FONT_HERSHEY_SIMPLEX, 1.5, (0,255,0),3)
    else :
	    # Mostra na tela a distancia (em px) horizontal da linha preta ao meio da imagem
        setpoint = 320
        error = centerx_blk - setpoint
        centertext = "Error = " + str(error)
        cv2.putText(image, centertext, (200,340), cv2.FONT_HERSHEY_SIMPLEX, 1.5, (255,0,0),3)

	# Mostra a imagem com o texto e a linha
    cv2.imshow("video", image)

    key = cv2.waitKey(1) & 0xFF
    if key == ord("q"):
        break

capture.release()
cv2.destroyAllWindows()