import time
import cv2
import numpy as np

# Cria a instância de captura das imagens da camera
capture = cv2.VideoCapture(0)

# Determina atributos da imagem
capture.set(cv2.CAP_PROP_FRAME_WIDTH, 640)  # Largura
capture.set(cv2.CAP_PROP_FRAME_HEIGHT, 480) # Altura
capture.set(cv2.CAP_PROP_FPS, 30)           # FPS
capture.set(cv2.CAP_PROP_BRIGHTNESS, 100)   # Luminosidade
capture.set(cv2.CAP_PROP_CONTRAST,150)      # Contraste

successful = True

# Laço principal (roda até haver uma falha na captura ou até que o usuário aperte 'q')
while successful:

    # Captura um frame da camera
    successful, image = capture.read()

    # Seleciona as áreas com cor preta (linha)
    Blackline = cv2.inRange(image, (0, 0, 0), (60, 60, 60))

    kernel = np.ones((3, 3), np.uint8)

    # Corta rúidos da seleção das cores pretas
    Blackline = cv2.erode(Blackline, kernel, iterations=5)
    Blackline = cv2.dilate(Blackline, kernel, iterations=9)

    # Contorna as áreas de seleção
    contours_blk, hierarchy_blk = cv2.findContours(Blackline, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    # roda caso haja contorno de seleção
    if len(contours_blk) > 0:

        # Retângulo mínimo que compõe a seleção
        blackbox = cv2.minAreaRect(contours_blk[0])
        (x_min, y_min), (w_min, h_min), ang = blackbox

        # Configuração do número do ângulo
        if ang < -45:
            ang = 90 + ang
        if w_min < h_min and ang > 0:
            ang = (90-ang)*-1
        if w_min > h_min and ang < 0:
            ang = 90 + ang
        ang = int(ang)

        # Mostra na tela a distancia (em px) horizontal da linha preta ao meio da imagem
        setpoint = 320
        error = int(x_min - setpoint)
        cv2.putText(image, str(error), (10, 320), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 2)
        cv2.line(image, (int(x_min), 200 ), (int(x_min), 250 ), (255,0,0),3)

        # Mostra na tela o ângulo e a caixa
        box = np.int0(cv2.boxPoints(blackbox))
        cv2.drawContours(image,[box],0,(0,0,255),2)
        cv2.putText(image, str(ang), (10, 40), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2)

    cv2.imshow("orginal with line", image)
    key = cv2.waitKey(1) & 0xFF
    if key == ord("q"):
        break

capture.release()
cv2.destroyAllWindows()
