import cv2
import numpy as np

FRAME_HEIGHT = 480
FRAME_WIDTH = 640

# Função para achar partes verdes da imagem

def trackObject(workImage):
    MINIMUM_GREEN_AREA = 7000
    contours, _ = cv2.findContours(
        workImage, cv2.RETR_CCOMP, cv2.CHAIN_APPROX_SIMPLE)

    greenContours = []
    numObjects = len(contours)

    if 100 > numObjects > 0:

        for i in range(numObjects):
            moment = cv2.moments(contours[i])
            green_area = moment["m00"]

            if green_area > MINIMUM_GREEN_AREA:
                x = moment["m10"] / green_area
                y = moment["m01"] / green_area

                greenContours.append((green_area, x, y))

    return np.round(sorted(greenContours, reverse=True)).astype("int")


# Desenhar ponteiro de quadrado verde

def drawObject(x, y, frame, color):
    cv2.circle(frame, (x, y), 10, color, 1)

    if(y-15 > 0):
        cv2.line(frame, (x, y), (x, y-15), color, 1)
    else:
        cv2.line(frame, (x, y), (x, 0), color, 1)

    if(y+15 < FRAME_HEIGHT):
        cv2.line(frame, (x, y), (x, y+15), color, 1)
    else:
        cv2.line(frame, (x, y), (x, FRAME_HEIGHT), color, 1)

    if(x-15 > 0):
        cv2.line(frame, (x, y), (x-15, y), color, 1)
    else:
        cv2.line(frame, (x, y), (0, y), color, 1)

    if(x+15 < FRAME_WIDTH):
        cv2.line(frame, (x, y), (x+15, y), color, 1)
    else:
        cv2.line(frame, (x, y), (FRAME_WIDTH, y), color, 1)


def scanline(img, Mp, line_radius):
    if len(img.shape) > 2:
        channels = img.shape[2]
    else:
        channels = 1

    nRows = FRAME_HEIGHT
    nCols = FRAME_WIDTH * channels
    scanpoint = Mp
    scan_radius = line_radius
    scan_w = line_radius * 2
    sc_strt = Mp[0] - line_radius
    sc_end = Mp[0] + line_radius
    