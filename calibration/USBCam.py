import threading
import time
import cv2
import numpy as np


class USBCam:
    def __init__(self, frame_w, frame_h, fps=60, src=0):
        self.capture = cv2.VideoCapture(src)

        self.capture.set(cv2.CAP_PROP_FRAME_WIDTH, frame_w)
        self.capture.set(cv2.CAP_PROP_FRAME_HEIGHT, frame_h)
        self.capture.set(cv2.CAP_PROP_FPS, fps)

        self.frame = []
        self.stopped = False
        self.paused = False

        self.new_frame = False

    def start(self):
        t = threading.Thread(target=self.update, args=())
        t.daemon = True
        t.start()
        return self

    def update(self):

        while True:

            if self.stopped:
                self.capture.release()
                return

            if not self.paused:
                _, self.frame = self.capture.read()
                self.new_frame = True

    def stop(self):
        self.stopped = True

    def pause(self, pause=True):
        self.paused = pause

    def getFrame(self):

        if self.new_frame:
            self.new_frame = False
            return self.frame

        return []