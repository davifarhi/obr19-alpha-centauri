from picamera.array import PiRGBArray
from picamera import PiCamera
import threading
import time
import cv2
import numpy as np


def imageAverage(image):
    image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    h = image.shape[0]
    w = image.shape[1]
    avg = 0

    for y in range(0, h, 8):
        for x in range(0, w, 8):
            avg += image[y][x]

    return avg/(h*w*64)


class RaspiCam:
    def __init__(self, frame_w, frame_h, fps=64):
        self.camera = PiCamera()

        self.camera.resolution = (frame_w, frame_h)
        self.camera.framerate = fps

        self.rawCapture = PiRGBArray(self.camera, size=(frame_w, frame_h))

        self.frame = []
        self.stopped = False
        self.paused = False

        self.new_frame = False

    def start(self):
        t = threading.Thread(target=self.update, args=())
        t.daemon = True
        t.start()
        return self

    def update(self):

#        self.time_last_brightness = time.time()
        for frame in self.camera.capture_continuous(self.rawCapture, format="bgr", use_video_port=True):
            if self.stopped:
                self.camera.close()
                return
            if not self.paused:
                self.frame = frame.array
                self.new_frame = True
                self.rawCapture.truncate(0)

#                if time.time() - self.time_last_brightness > 1:
#                    self.setBrightness()

    def setBrightness(self):
        avg = imageAverage(self.frame)
        if avg > 125:
            if self.camera.brightness > 0:
                self.camera.brightness -= 3
        elif avg < 115:
            if self.camera.brightness < 100:
                self.camera.brightness += 3
        self.time_last_brightness = time.time()

    def stop(self):

        self.stopped = True

    def pause(self, pause=True):
        self.paused = pause

    def getFrame(self):

        if self.new_frame:
            self.new_frame = False
            return self.frame

        return []
