import pigpio


class Digital_GPIO:
    def __init__(self, io_mode, pigpio_obj, pin, invert=False):
        if io_mode != "input" and io_mode != "output":
            raise ValueError(
                "Modo de GPIO é inapropriado, deve ser 'input' ou 'output'")
        self.io = pigpio_obj
        self.mode = io_mode
        self.pin = pin
        self.io.set_mode(pin, pigpio.OUTPUT if io_mode ==
                         "output" else pigpio.INPUT)

        self.invert = invert

    def write(self, value):
        if self.mode != "output":
            raise ValueError(
                "Apenas porta configurada como 'output' pode ser usada para escrita")
        if self.invert:
            value = not value
        self.io.write(self.pin, value)

    def read(self):
        if self.mode != "input":
            raise ValueError(
                "Apenas porta configurada como 'input' pode ser usada para leitura")

        value = self.io.read(self.pin)
        if self.invert:
            value = not value

        return value
