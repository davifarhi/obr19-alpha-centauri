import pigpio
from H_Bridge import H_Bridge
import Adafruit_PCA9685

L298N_MOTORS = {
    "right_pins": [19, 13],
    "left_pins": [6, 5],
    "right_pwm": 0,
    "left_pwm": 1
}

gpio = pigpio.pi()
pca = Adafruit_PCA9685.PCA9685()

pca.set_pwm_freq(100)

h_bridge = H_Bridge(L298N_MOTORS, gpio, pca)

val1 = 0
val2 = 0

try:
    while True:
        val1 = int(input("Valor 1: "))
        val2 = int(input("Valor 2: "))

        if abs(val1) > 100:
            print("Valor 1 inadequado")
            val1 = 0

        if abs(val2) > 100:
            print("Valor 1 inadequado")
            val1 = 0

        h_bridge.run(val1, val2)

except KeyboardInterrupt:
    pass

h_bridge.reset()
gpio.stop()