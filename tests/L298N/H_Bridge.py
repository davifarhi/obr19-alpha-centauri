# https://geextor.com/2016/11/20/driving-a-dc-motor-with-raspberry-pi/
import pigpio
import time


def map(x, in_min, in_max, out_min, out_max):
    return int(round((x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min))


class H_Bridge:
    def __init__(self, pins_obj, gpio_obj, pca9685_obj):
        self.pca = pca9685_obj
        self.io = gpio_obj

        self.motor_right = pins_obj["right_pins"]
        self.motor_left = pins_obj["left_pins"]

        self.io.set_mode(self.motor_right[0], pigpio.OUTPUT)
        self.io.set_mode(self.motor_right[1], pigpio.OUTPUT)
        self.io.set_mode(self.motor_left[0], pigpio.OUTPUT)
        self.io.set_mode(self.motor_left[1], pigpio.OUTPUT)

        self.pin_R_pwm = pins_obj["right_pwm"]
        self.pin_L_pwm = pins_obj["left_pwm"]

        self.multiplier = 1.0
        self.right_multiplier = 0.7
        self.left_multiplier = 1.0

        self.reset()

    def setMultiplier(self, value):
        if 0 <= value <= 1:
            self.multiplier = value
        else:
            raise ValueError("Multipler value cannot be smaller than 0 or bigger than 1")

    def reset(self):
        self.io.write(self.motor_right[0], 0)
        self.io.write(self.motor_right[1], 0)

        self.io.write(self.motor_left[0], 0)
        self.io.write(self.motor_left[1], 0)

        self.pca.set_pwm(self.pin_R_pwm, 0, 0)
        self.pca.set_pwm(self.pin_L_pwm, 0, 0)

    def run(self, pwm_R, pwm_L):
        forward_R = (pwm_R >= 0)
        forward_L = (pwm_L >= 0)

        pwm_R = abs(pwm_R) * self.multiplier * self.right_multiplier
        pwm_L = abs(pwm_L) * self.multiplier * self.left_multiplier

        if not pwm_L and not pwm_R:

            self.io.write(self.motor_right[0], 0)
            self.io.write(self.motor_right[1], 0)

            self.io.write(self.motor_left[0], 0)
            self.io.write(self.motor_left[1], 0)

        else:
            if forward_R:
                self.io.write(self.motor_right[0], 1)
                self.io.write(self.motor_right[1], 0)
            else:
                self.io.write(self.motor_right[0], 0)
                self.io.write(self.motor_right[1], 1)

            if forward_L:
                self.io.write(self.motor_left[0], 1)
                self.io.write(self.motor_left[1], 0)
            else:
                self.io.write(self.motor_left[0], 0)
                self.io.write(self.motor_left[1], 1)

        self.pca.set_pwm(self.pin_R_pwm, 0, map(pwm_R, 0, 100, 0, 0xfff))
        self.pca.set_pwm(self.pin_L_pwm, 0, map(pwm_L, 0, 100, 0, 0xfff))

    def turn90(self, direction):
        prev_mult = self.multiplier
        self.setMultiplier(1)

        if direction == 'left':
            self.io.write(self.motor_left[0], 1)
            self.io.write(self.motor_left[1], 0)

            self.io.write(self.motor_right[0], 0)
            self.io.write(self.motor_right[1], 1)

        if direction == 'right':
            self.io.write(self.motor_left[0], 0)
            self.io.write(self.motor_left[1], 1)

            self.io.write(self.motor_right[0], 1)
            self.io.write(self.motor_right[1], 0)

        self.pca.set_pwm(self.pin_R_pwm, 0, 0xfff)
        self.pca.set_pwm(self.pin_L_pwm, 0, 0xfff)

        time.sleep(0.4)

        self.reset()
        self.setMultiplier(prev_mult)

    def turn180(self, direction):
        prev_mult = self.multiplier
        self.setMultiplier(1)

        if direction == 'left':
            self.io.write(self.motor_left[0], 1)
            self.io.write(self.motor_left[1], 0)

            self.io.write(self.motor_right[0], 0)
            self.io.write(self.motor_right[1], 1)

        if direction == 'right':
            self.io.write(self.motor_left[0], 0)
            self.io.write(self.motor_left[1], 1)

            self.io.write(self.motor_right[0], 1)
            self.io.write(self.motor_right[1], 0)

        self.pca.set_pwm(self.pin_R_pwm, 0, 0xfff)
        self.pca.set_pwm(self.pin_L_pwm, 0, 0xfff)

        time.sleep(0.8)

        self.reset()
        self.setMultiplier(prev_mult)
