import pigpio
import threading
import time

# usage
# USsensor = UltrassonicSensor(io, 17, 27).start()


class UltrassonicSensor:
    def __init__(self, pigpio_obj, trigger_pin, echo_pin):
        self.echo_pin = echo_pin
        self.trigger_pin = trigger_pin

        self.io = pigpio_obj

        self.stopped = False
        self.paused = False

        self.distance = None

        self.io.set_mode(self.echo_pin, pigpio.INPUT)
        self.io.set_mode(self.trigger_pin, pigpio.OUTPUT)

    def start(self):
        t = threading.Thread(target=self.update, args=())
        t.daemon = True
        t.start()
        return self

    def update(self):

        while True:
            if self.stopped:
                return
            if not self.paused:
                self.distance = self.get()
                time.sleep(0.05)

    def stop(self):
        self.stopped = True

    def pause(self, pause=True):
        self.paused = pause

    def get(self):

        pulse_start = None
        pulse_end = None

        self.io.write(self.trigger_pin, True)
        time.sleep(0.00001)
        self.io.write(self.trigger_pin, False)

        loop_start = time.time()
        while self.io.read(self.echo_pin) == 0:
            pulse_start = time.time()
            if pulse_start - loop_start > 0.02:
                return

        if not pulse_start:
            return

        loop_start = time.time()
        while self.io.read(self.echo_pin) == 1:
            pulse_end = time.time()
            if pulse_end - loop_start > 0.02:
                return

        if not pulse_end:
            return

        return round((pulse_end - pulse_start) * 17160.5, 2)
