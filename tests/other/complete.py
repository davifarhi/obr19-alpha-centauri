import time
import math
import numpy as np
import cv2
import tempo

# ====== CONSTANTES ======

# 1 = 640x480, 2 = 320x240, 4 = 160x120
PX_DIVISOR = 1

# Dimensões da imagem
FRAME_WIDTH = int(640 / PX_DIVISOR)
FRAME_HEIGHT = int(480 / PX_DIVISOR)

# Max e Min para seleção de áreas pretas
BLACK_BGR_MINIMUM = (0, 0, 0)
BLACK_BGR_MAXIMUM = (40, 40, 40)

# Configuração para erosão de seleção de áreas pretas
BLACK_ERODE_KERNEL = np.ones((3, 3), np.uint8)
BLACK_ERODE_ITERATIONS = int(2 / PX_DIVISOR)

# Configuração de tolerância para identificação de linhas
PIXEL_TOLERANCE = int(8 / PX_DIVISOR)
LINES_DIFFERENCE_MAX_X = int(100 / PX_DIVISOR)

# Min e Max de largura para as linhas
MINIMUM_LINE_WIDTH = int(20 / PX_DIVISOR)
MAXIMUM_LINE_WIDTH = int(120 / PX_DIVISOR)
DP_L_R_MULT = 4

# Configurações para seleção do verde
HSV_MINIMUM = (0, 71, 0)
HSV_MAXIMUM = (87, 256, 256)
MINIMUM_GREEN_AREA = int(3000 / math.pow(PX_DIVISOR, 2))
GREEN_ITERATIONS_NUMBER = int(2 / PX_DIVISOR)
GREEN_ERODE_KERNEL = np.ones((3, 3), np.uint8)


# ====== INSTÂNCIAS ======

CAPTURE = cv2.VideoCapture(0)

FPS = tempo.Framerate()

# ====== CONFIGURAÇÔES ======

# Determina atributos da imagem
CAPTURE.set(cv2.CAP_PROP_FRAME_WIDTH, FRAME_WIDTH)  # Largura
CAPTURE.set(cv2.CAP_PROP_FRAME_HEIGHT, FRAME_HEIGHT)  # Altura
CAPTURE.set(cv2.CAP_PROP_FPS, 30)           # FPS
CAPTURE.set(cv2.CAP_PROP_BRIGHTNESS, 100)   # Luminosidade
CAPTURE.set(cv2.CAP_PROP_CONTRAST, 150)      # Contraste

# ====== FUNÇÕES ======


def getAngle(p0, p1):
    """Calcula o ângulo tangente entre os dois pontos"""
    try:
        return int(round(math.degrees(math.atan((p1[0] - p0[0]) / (p0[1] - p1[1])))))
    except ZeroDivisionError:
        return 0


def linesDifference(this_line, last_line):
    """Calcula por quantos pixels as duas linhas estão separadas
    (Calculado a partir da ponta de cada linha"""

    this_start = this_line[0] - int(this_line[1]/2)
    this_end = this_line[0] + int(this_line[1]/2)

    last_start = last_line[0] - int(last_line[1]/2)
    last_end = last_line[0] + int(last_line[1]/2)

    if this_end < last_start:
        return last_start - this_end
    if last_end < this_start:
        return this_start - last_end
    return 0


def rowScan(row, compare_x, compare_w):
    """Busca por uma linha no vetor de bits"""
    start = None
    end = None
    tolerance = 0
    line = []
    found_line = False

    for i, value in enumerate(row):
        if value:
            if not start:
                start = i

            tolerance = PIXEL_TOLERANCE + 1
            end = i

        elif start and tolerance:
            tolerance = tolerance - 1

        if start and ((not tolerance and not value) or i == (len(row)-1)):
            if end - start >= MINIMUM_LINE_WIDTH:
                line_x = int(round((start+end)/2))
                line_w = end - start + 1
                line.append((abs(compare_x - line_x), line_x, line_w))
                found_line = True

            end = None
            start = None

    if found_line:
        line = min(line)
        line = line[:][1:]
        if linesDifference(line, (compare_x, compare_w)) <= LINES_DIFFERENCE_MAX_X:
            return line
    return False, False


def greenScan(image):
    """Função que seleciona as áreas verdes na imagem e identifica contornos"""
    hsv_image = cv2.cvtColor(image, cv2.COLOR_RGB2HSV)

    bin_img = cv2.inRange(hsv_image, HSV_MINIMUM, HSV_MAXIMUM)
    bin_img = cv2.erode(
        bin_img, GREEN_ERODE_KERNEL, iterations=GREEN_ITERATIONS_NUMBER)

    contours, _ = cv2.findContours(
        bin_img, cv2.RETR_CCOMP, cv2.CHAIN_APPROX_SIMPLE)

    green_contours = []

    if len(contours):

        for contour in contours:
            moment = cv2.moments(contour)
            green_area = moment["m00"]

            if green_area > MINIMUM_GREEN_AREA:
                x = moment["m10"] / green_area
                y = moment["m01"] / green_area

                green_contours.append((green_area, x, y))

        green_contours = np.round(
            sorted(green_contours, reverse=True)).astype("int")

    if not len(green_contours):
        return []
    if len(green_contours) > 4:
        green_contours = green_contours[0:4]

    return green_contours


def scanLeftRight(bin_img, intersection):
    """Scaneia os dois lados da intersecção para identificar se há
    um caminho ou não

    Retorna booleano "direita" e "esquerda" """

    first_y, first_w = rowScan(
        [row[intersection[0]] for row in bin_img], intersection[1], const.DP_LINE_WIDTH_MAXIMUM)

    if not first_w:
        return False, False

    prev_y = first_y
    prev_w = first_w

    right = False
    left = False

    line_counter = 0

    limit = intersection[0] - 1 - const.DP_LINE_WIDTH_MAXIMUM
    if limit < 0:
        limit = 0
    for x in reversed(range(limit, intersection[0])):
        line_center, line_width = rowScan(
            [row[x] for row in bin_img], prev_y, prev_w)
        if 0 < line_width < const.DP_LINE_WIDTH_MAXIMUM:
            if not linesDifference(
                    (line_center, line_width), (first_y, first_w)):
                if line_counter >= const.DP_LINE_WIDTH_MINIMUM * DP_L_R_MULT:
                    left = True
                    break
                line_counter = line_counter + 1
                prev_y = line_center
                prev_w = line_width

        else:
            line_counter = 0

    prev_y = first_y
    prev_w = first_w

    line_counter = 0

    limit = intersection[0] + 1 + const.DP_LINE_WIDTH_MAXIMUM
    if limit >= const.FRAME_WIDTH:
        limit = const.FRAME_WIDTH - 1
    for x in range(intersection[0] + 1, limit):
        line_center, line_width = rowScan(
            [row[x] for row in bin_img], prev_y, prev_w)

        if 0 < line_width < const.DP_LINE_WIDTH_MAXIMUM:
            if not linesDifference(
                    (line_center, line_width), (first_y, first_w)):
                if line_counter >= const.DP_LINE_WIDTH_MINIMUM * DP_L_R_MULT:
                    right = True
                    break
                line_counter = line_counter + 1
                prev_y = line_center
                prev_w = line_width

        else:
            line_counter = 0

    return left, right


def drawTarget(image, x, y, color):
    """Desenha uma retícula no ponto indicado"""
    cv2.circle(image, (x, y), 10, color, 1)

    if(y-15 > 0):
        cv2.line(image, (x, y), (x, y-15), color, 1)
    else:
        cv2.line(image, (x, y), (x, 0), color, 1)
    if(y+15 < FRAME_HEIGHT):
        cv2.line(image, (x, y), (x, y+15), color, 1)
    else:
        cv2.line(image, (x, y), (x, FRAME_HEIGHT), color, 1)
    if(x-15 > 0):
        cv2.line(image, (x, y), (x-15, y), color, 1)
    else:
        cv2.line(image, (x, y), (0, y), color, 1)
    if(x+15 < FRAME_WIDTH):
        cv2.line(image, (x, y), (x+15, y), color, 1)
    else:
        cv2.line(image, (x, y), (FRAME_WIDTH, y), color, 1)

    return image


def detectPath(image, prev_line):
    """Scaneia a imagem em busca de linhas e intersecções"""

    black_selection = cv2.inRange(image, BLACK_BGR_MINIMUM, BLACK_BGR_MAXIMUM)
    black_selection = cv2.erode(
        black_selection, BLACK_ERODE_KERNEL, iterations=BLACK_ERODE_ITERATIONS)

    first_line = []
    first_line_width = False
    intersection = {}
    intersection_line_center = 0
    green = False
    max_width_line_counter = 0

    for y in reversed(range(FRAME_HEIGHT)):

        line_center, line_width = rowScan(
            black_selection[y][:], prev_line[0], prev_line[1])

        if line_width:

            if "y" in intersection and max_width_line_counter > MINIMUM_LINE_WIDTH * 2 and line_width <= MAXIMUM_LINE_WIDTH:
                intersection["x"] = int((prev_line[0] + line_center) / 2)
                intersection["front"] = True
                break

            if line_width > MAXIMUM_LINE_WIDTH:
                intersection_line_center = line_center
                if not intersection:
                    if max_width_line_counter >= MINIMUM_LINE_WIDTH * 2 / 3 and first_line:
                        intersection["y"] = y
                        max_width_line_counter = 0
                    else:
                        max_width_line_counter = max_width_line_counter + 1
            else:
                if intersection:
                    max_width_line_counter = max_width_line_counter + 1
                else:
                    prev_line = [line_center, y]
                    max_width_line_counter = 0

                if not first_line:
                    first_line = [line_center, y]
                    first_line_width = line_width

    if intersection:
        intersection["left"] = False
        intersection["right"] = False
        if not "front" in intersection:
            intersection["front"] = False
        if not "x" in intersection:
            intersection["x"] = prev_line[0]
            
        intersection["left"], intersection["right"] = scanLeftRight(
            black_selection, (intersection["x"], intersection["y"]))

    return first_line, prev_line, first_line_width, intersection


# ====== MAIN LOOP ======

if __name__ == "__main__":
    last_bottom_line = [int(FRAME_WIDTH/2), FRAME_WIDTH]

    while True:
       # _, frame = CAPTURE.read()
        frame = cv2.imread("fotos/foto_19.jpg")
        frame = cv2.resize(frame, (FRAME_WIDTH, FRAME_HEIGHT))
        output = frame.copy()

        first_point, last_point, fp_width, intersection = detectPath(
            frame, last_bottom_line)

        if fp_width:
            last_bottom_line = [first_point[0], fp_width]

            if intersection:

                green = greenScan(frame)

                if len(green):
                    for (_, x, y) in green:
                        output = drawTarget(output, x, y, (0, 255, 0))
                else:
                    cv2.putText(output, "No green", (10, 90),
                                cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2)

                output = cv2.circle(
                    output, (intersection["x"], intersection["y"]), 2, (255, 255, 255), 4)
                if intersection["front"]:
                    cv2.putText(output, "F", (FRAME_WIDTH - 18, 20),
                                cv2.FONT_HERSHEY_SIMPLEX, 0.7, (255, 0, 0), 2)
                if intersection["left"]:
                    cv2.putText(output, "L", (FRAME_WIDTH - 18, 40),
                                cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 255, 0), 2)
                if intersection["right"]:
                    cv2.putText(output, "R", (FRAME_WIDTH - 18, 60),
                                cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 0, 255), 2)
            else:
                cv2.putText(output, "No intersection", (10, 90),
                            cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2)

            output = cv2.line(output, tuple(first_point),
                              tuple(last_point), (0, 255, 0), 3)

            ang = getAngle(first_point, last_point)
            offcenter = int(
                round(((first_point[0] + last_point[0]) / 2) - FRAME_WIDTH/2))

            cv2.putText(output, str(ang)+"deg", (10, 30),
                        cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)
            cv2.putText(output, str(offcenter)+"px", (10, 60),
                        cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)
        else:
            cv2.putText(output, "No Line", (10, 30),
                        cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2)
        cv2.imshow("", output)
        FPS.setNewFrame()
        if cv2.waitKey(1) & 0xFF == ord("q"):
            break

print("FPS: "+str(FPS.finalFPS()))
CAPTURE.release()
cv2.destroyAllWindows()
