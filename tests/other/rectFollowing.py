import time
import cv2
import numpy as np
import tempo
import math

capture = cv2.VideoCapture(0)

fps = tempo.Framerate()

PX_DIVISOR = 1

FRAME_WIDTH = int(640 / PX_DIVISOR)
FRAME_HEIGHT = int(480 / PX_DIVISOR)

# Determina atributos da imagem
capture.set(cv2.CAP_PROP_FRAME_WIDTH, FRAME_WIDTH)  # Largura
capture.set(cv2.CAP_PROP_FRAME_HEIGHT, FRAME_HEIGHT)  # Altura
capture.set(cv2.CAP_PROP_FPS, 30)           # FPS
capture.set(cv2.CAP_PROP_BRIGHTNESS, 100)   # Luminosidade
capture.set(cv2.CAP_PROP_CONTRAST, 150)      # Contraste

BLACK_BGR_MINIMUM = (0, 0, 0)
BLACK_BGR_MAXIMUM = (75, 75, 75)
BLACK_ERODE_KERNEL = np.ones((3, 3), np.uint8)
BLACK_ERODE_ITERATIONS = int(5 / PX_DIVISOR)
BLACK_MINIMUM_AREA = int(1000 / PX_DIVISOR)

PX_TO_DETECT_GAP = FRAME_HEIGHT * 4/5

def calculateAngle(angle, width, height):
    # A função reporta corretamento os ângulos exceto na situação de uma curva de 90°, nesse caso o angulo varia bastante !!atenção!!
    if angle < -45:
        angle = 90 + angle
    if width < height and angle > 0:
        angle = (90-angle)*-1
    if width > height and angle < 0:
        angle = 90 + angle

    return int(round(angle))

def getDistance(point0, point1):
    return int(round(math.sqrt(math.pow(point0[0] - point1[0], 2) + math.pow(point0[1] - point1[1], 2))))

def pathScan(image):

    black_rect = None
    blackline = cv2.inRange(image, BLACK_BGR_MINIMUM, BLACK_BGR_MAXIMUM)
    blackline = cv2.erode(blackline, BLACK_ERODE_KERNEL,
                          iterations=BLACK_ERODE_ITERATIONS)

    contours, _ = cv2.findContours(
        blackline.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    new_contours = []

    for contour in contours:
        moment = cv2.moments(contour)
        if moment["m00"] >= BLACK_MINIMUM_AREA:
            new_contours.append(contour)

    contours = new_contours
    new_contours = []
    contours_len = len(contours)

    if contours_len > 0:

        if contours_len == 1:
            return cv2.minAreaRect(contours[0])

        else:
            bottom_contours = []

            for i in range(contours_len):
                black_rect = cv2.minAreaRect(contours[i])
                box = cv2.boxPoints(black_rect)
                (_, y_box) = box[0]

                if y_box > FRAME_HEIGHT - 2:
                    bottom_contours.append(black_rect)
                else:
                    new_contours.append(black_rect)

            if len(bottom_contours) == 1:
                return bottom_contours[0]

            elif len(bottom_contours) == 0:
                candidates = []
                for i in range(contours_len):
                    rect_center, _, _ = new_contours[i]
                    distance = getDistance(rect_center, prev_rect_center)

                candidates.append((distance, i))
                return new_contours[candidates[0][1]]

            else:
                candidates_off_bottom = []
                for i in range(len(bottom_contours)):
                    rect_center, _, _ = bottom_contours[i]
                    distance = getDistance(rect_center, prev_rect_center)
                    candidates_off_bottom.append((distance, i))

                candidates_off_bottom.sort()
                return bottom_contours[candidates_off_bottom[0][1]]

    else:
        return False


prev_rect_center = [int(FRAME_WIDTH/2), int(FRAME_HEIGHT/2)]

while True:
    _, frame = capture.read()

    black_rect = pathScan(frame)

    if black_rect:
        box = cv2.boxPoints(black_rect)
        _, height = box[2]
        box = np.int0(box)
        frame = cv2.drawContours(frame, [box], 0, (0, 0, 255), 2)
        (x, y), (w, h), ang = black_rect
        prev_rect_center = [x, y]

        if height >= PX_TO_DETECT_GAP:
            gap_detected = True
            cv2.putText(frame, "Gap detected", (10, 90),
                        cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)

        ang = calculateAngle(int(ang), w, h)
        offcenter = int(round(x - FRAME_WIDTH/2))

        cv2.putText(frame, str(ang)+"deg", (10, 30),
                    cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)
        cv2.putText(frame, str(offcenter)+"px", (10, 60),
                    cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)

    else:
        cv2.putText(frame, "No line", (10, 40),
                    cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2)

    cv2.imshow("", frame)
    fps.setNewFrame()
    key = cv2.waitKey(1) & 0xFF
    if key == ord("q"):
        break

print("FPS: "+str(fps.finalFPS()))
capture.release()
cv2.destroyAllWindows()
