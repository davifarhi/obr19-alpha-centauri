import time
import math
import cv2
import numpy as np
import tempo

# Cria a instância de captura das imagens da camera
CAPTURE = cv2.VideoCapture(0)

FPS = tempo.Framerate()

PX_DIVISOR = 1

FRAME_WIDTH = int(640 / PX_DIVISOR)
FRAME_HEIGHT = int(480 / PX_DIVISOR)

# Determina atributos da imagem
CAPTURE.set(cv2.CAP_PROP_FRAME_WIDTH, FRAME_WIDTH)  # Largura
CAPTURE.set(cv2.CAP_PROP_FRAME_HEIGHT, FRAME_HEIGHT)  # Altura
CAPTURE.set(cv2.CAP_PROP_FPS, 30)           # FPS
CAPTURE.set(cv2.CAP_PROP_BRIGHTNESS, 100)   # Luminosidade
CAPTURE.set(cv2.CAP_PROP_CONTRAST, 150)      # Contraste

BLACK_BGR_MINIMUM = (0, 0, 0)
BLACK_BGR_MAXIMUM = (12, 14, 23)
BLACK_ERODE_KERNEL = np.ones((3, 3), np.uint8)
BLACK_ERODE_ITERATIONS = int(2 / PX_DIVISOR)

SCAN_Y_DELTA = int(8 / PX_DIVISOR)
MINIMUM_LINE_WIDTH = int(52 / PX_DIVISOR)
PIXEL_TOLERANCE = int(8 / PX_DIVISOR)

MAXIMUM_LINE_WIDTH = int(150 / PX_DIVISOR)

HSV_MINIMUM = (0, 71, 10)
HSV_MAXIMUM = (87, 256, 256)
MINIMUM_GREEN_AREA = int(3000 / math.pow(PX_DIVISOR, 2))
GREEN_ITERATIONS_NUMBER = int(2 / PX_DIVISOR)
GREEN_ERODE_KERNEL = np.ones((3, 3), np.uint8)


def greenScan(image):

    hsv_image = cv2.cvtColor(image, cv2.COLOR_RGB2HSV)

    thresh_image = cv2.inRange(hsv_image, HSV_MINIMUM, HSV_MAXIMUM)
    eroded_image = cv2.erode(
        thresh_image, GREEN_ERODE_KERNEL, iterations=GREEN_ITERATIONS_NUMBER)

    contours, _ = cv2.findContours(
        eroded_image, cv2.RETR_CCOMP, cv2.CHAIN_APPROX_SIMPLE)

    green_contours = []

    minimum_y = 0

    if len(contours):

        for i in range(len(contours)):
            moment = cv2.moments(contours[i])
            green_area = moment["m00"]

            if green_area > MINIMUM_GREEN_AREA:
                x = moment["m10"] / green_area
                y = moment["m01"] / green_area

                min_y = tuple(contours[i][contours[i][:, :, 1].argmax()][0])[1]
                if min_y > minimum_y:
                    minimum_y = min_y

                green_contours.append((green_area, x, y))

        green_contours = np.round(
            sorted(green_contours, reverse=True)).astype("int")

    if not len(green_contours):
        return None, None
    if len(green_contours) > 4:
        green_contours = green_contours[0:4]

    return green_contours, minimum_y

def drawTarget(image, x, y, color):
    cv2.circle(image, (x, y), 10, color, 1)

    if(y-15 > 0):
        cv2.line(image, (x, y), (x, y-15), color, 1)
    else:
        cv2.line(image, (x, y), (x, 0), color, 1)
    if(y+15 < FRAME_HEIGHT):
        cv2.line(image, (x, y), (x, y+15), color, 1)
    else:
        cv2.line(image, (x, y), (x, FRAME_HEIGHT), color, 1)
    if(x-15 > 0):
        cv2.line(image, (x, y), (x-15, y), color, 1)
    else:
        cv2.line(image, (x, y), (0, y), color, 1)
    if(x+15 < FRAME_WIDTH):
        cv2.line(image, (x, y), (x+15, y), color, 1)
    else:
        cv2.line(image, (x, y), (FRAME_WIDTH, y), color, 1)

    return image

def intersectionRowScan(row, comp_x):
    start = None
    end = None
    tolerance = 0
    lines = []
    foundLine = False
    
    for i in range(len(row)):
        if row[i]:
            if not start:
                start = i
            
            tolerance = PIXEL_TOLERANCE + 1
            end = i

        elif start and tolerance:
            tolerance = tolerance - 1

        if start and ((not tolerance and not row[i]) or i == (len(row)-1)):
            if end - start >= MINIMUM_LINE_WIDTH:
                centerX = int(round((start+end)/2))
                lines.append((abs(comp_x - centerX), centerX, end - start + 1))
                foundLine = True

            end = None
            start = None

    if foundLine:
        line = min(lines)
        return line[:][1:]
    else:
        return False, False


def findIntersection(image, avg_x, avg_y):
    intersection_center = []
    intersection_paths = [False, False, False] # right, front, left

    blackline = cv2.inRange(image, BLACK_BGR_MINIMUM, BLACK_BGR_MAXIMUM)
    blackline = cv2.erode(blackline, BLACK_ERODE_KERNEL,
                          iterations=BLACK_ERODE_ITERATIONS)

    up_x = avg_x
    down_x = avg_x
    i = 0

    while i < 25:
        line_down = False
        line_up = False

        if avg_y + i * SCAN_Y_DELTA < FRAME_HEIGHT:
            line_down = intersectionRowScan(blackline[avg_y + i * SCAN_Y_DELTA][:], down_x)
        if avg_y - i * SCAN_Y_DELTA >= 0:
            line_up = intersectionRowScan(blackline[avg_y - i * SCAN_Y_DELTA][:], up_x)

        if line_down:
            y = avg_y + i * SCAN_Y_DELTA
            line_center, line_width = line_down
            output = cv2.line(image, (line_center - int(line_width/2), y),(line_center + int(line_width/2), y), (0,0,255), 1)
            if line_width > MAXIMUM_LINE_WIDTH:

                if i > 0:
                    j = 0
                    while line_width > MAXIMUM_LINE_WIDTH:
                        j = j + 1
                        if y + j * SCAN_Y_DELTA < FRAME_HEIGHT:
                            line_center, line_width = intersectionRowScan(blackline[y + j * SCAN_Y_DELTA][:], down_x)
                        else: 
                            line_center, line_width = intersectionRowScan(blackline[y - j * SCAN_Y_DELTA][:], up_x)

                intersection_center = [line_center, y]
                break

            elif line_width > MINIMUM_LINE_WIDTH:
                down_x = line_center

        if line_up:
            line_center, line_width = line_up
            y = avg_y - i * SCAN_Y_DELTA
            output = cv2.line(image, (line_center - int(line_width/2), y),(line_center + int(line_width/2), y), (0,0,255), 1)
            if line_width > MAXIMUM_LINE_WIDTH:
                y = avg_y - i * SCAN_Y_DELTA

                if i > 0:
                    j = 0
                    while line_width > MAXIMUM_LINE_WIDTH:
                        j = j + 1
                        if y - j * SCAN_Y_DELTA >= 0:
                            line_center, line_width = intersectionRowScan(blackline[y - j * SCAN_Y_DELTA][:], down_x)
                        else:
                            line_center, line_width = intersectionRowScan(blackline[y + j * SCAN_Y_DELTA][:], up_x)

                intersection_center = [line_center, y]
                break

            elif line_width > MINIMUM_LINE_WIDTH:
                up_x = line_center
                

        i = i + 1

    return intersection_center, output

    



while True:
    _, frame = CAPTURE.read()
    output = frame.copy()

    GREEN_OBJECTS, lowest_y = greenScan(frame)

    if GREEN_OBJECTS is not None:
        avg_x = 0
        avg_y = 0
        for (_, x, y) in GREEN_OBJECTS:
            avg_x = avg_x + x
            avg_y = avg_y + y
            output = drawTarget(frame, x, y, (0, 255, 0))

        avg_x = int(round(avg_x / len(GREEN_OBJECTS)))
        avg_y = int(round(avg_y / len(GREEN_OBJECTS)))

        intersection, output = findIntersection(frame, avg_x, avg_y)
        if intersection:
            frame = cv2.circle(frame, (intersection[0], intersection[1]), 3, (255,255,255), 2)

    else:
        pass

    cv2.imshow("", frame)
    FPS.setNewFrame()
    if cv2.waitKey(1) & 0xFF == ord("q"):
        break

print("FPS: "+str(FPS.finalFPS()))
CAPTURE.release()
cv2.destroyAllWindows()
