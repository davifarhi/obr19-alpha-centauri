from adafruit_servokit import ServoKit
from Adafruit_PCA9685 import PCA9685

pca = PCA9685()

kit = ServoKit(channels=16)

pca.set_pwm_freq(100)

SERVO_PIN = 7
SERVO_W_RANGE = [500, 2400]
START_ANG = 40

servo = kit.servo[SERVO_PIN]
servo.set_pulse_width_range(SERVO_W_RANGE[0],SERVO_W_RANGE[1])
servo.angle = START_ANG

try:
    while True:
        ang = int(input("Insira um angulo: "))

        servo.angle = ang

except KeyboardInterrupt:
    print("terminando...")
    servo.angle = None
