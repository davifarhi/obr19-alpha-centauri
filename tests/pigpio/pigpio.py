# pigpiotest.py
# É necessário instalar o pigpio e executá-lo em segundo plano sudo pigpiod

import time
import pigpio


def map(x, in_min, in_max, out_min, out_max):
    return round((x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min)


LED_PIN = 24
SERVO_PIN = 23
pi = pigpio.pi()

pi.set_mode(LED_PIN, pigpio.OUTPUT)
pi.set_mode(SERVO_PIN, pigpio.OUTPUT)

try:
    while True:
        pi.write(LED_PIN, 1)
        time.sleep(1.5)
        pi.write(LED_PIN, 0)

        for i in range(101):
            if i % 2 == 0: print(i)
            pi.set_PWM_dutycycle(LED_PIN, map(i, 0, 100, 0, 255))
            pi.set_servo_pulsewidth(SERVO_PIN, map(i, 0, 100, 500, 2500))
            time.sleep(0.05)
            
        pi.set_servo_pulsewidth(SERVO_PIN,0)


except KeyboardInterrupt:
    pass

pi.set_PWM_dutycycle(LED_PIN, 0)
pi.set_servo_pulsewidth(SERVO_PIN, 0)
pi.stop()
